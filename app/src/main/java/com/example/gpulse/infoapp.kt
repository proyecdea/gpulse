package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_configuracion.*
import kotlinx.android.synthetic.main.activity_infoapp.*

class infoapp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_infoapp)

        button1.setOnClickListener{
            val intent: Intent = Intent(this, Configuracion::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
    }

    override fun onBackPressed() {
        val intent: Intent = Intent(this, Configuracion::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
    }
}
