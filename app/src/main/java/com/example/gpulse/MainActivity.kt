package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            val intent:Intent = Intent(this, Ingresar::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
        button2.setOnClickListener{
            val intent:Intent = Intent(this, Registrarse::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
    }

    override fun onBackPressed() {
        val intent: Intent= Intent (Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

}
