package com.example.gpulse

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_registrarse.*
import kotlinx.android.synthetic.main.activity_seleccionar_cuenta.*

class seleccionar_cuenta : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seleccionar_cuenta)

        button7.setOnClickListener{
            val intent: Intent = Intent(this, Registrarse::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)

        }

        button8.setOnClickListener{
            val intent: Intent = Intent(this, contactosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fadein,R.anim.fadeout)

        }

        button9.setOnClickListener{
            val intent: Intent = Intent(this, usuariosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fadein,R.anim.fadeout)

        }
        button10.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Usuario con pulsera")
            builder.setMessage("Los usuarios con pulsera podrán vincular contactos para recibir la alerta de la pulsera vinculada")
            builder.setNeutralButton("Regresar", { dialog: DialogInterface?, which: Int -> })
            builder.show()
        }

        button11.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Contacto de confianza")
            builder.setMessage("Los contactos de confianza podrán registrar usuarios con pulsera para recibir sus alertas y poder monitorear su ubicación")
            builder.setNeutralButton("Regresar", { dialog: DialogInterface?, which: Int -> })
            builder.show()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent: Intent = Intent(this, Registrarse::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
    }
}
