package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_perfil.*

class perfil : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)

        button12.setOnClickListener{
            val intent: Intent = Intent(this, contactosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button13.setOnClickListener{
            val intent: Intent = Intent(this, usuariosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button14.setOnClickListener{
            val intent: Intent = Intent(this, mapa::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button16.setOnClickListener{
            val intent: Intent = Intent(this, notificaciones::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }

        button1.setOnClickListener{
            val intent: Intent = Intent(this, Configuracion::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
    }
    override fun onBackPressed() {
        val intent: Intent= Intent (Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
