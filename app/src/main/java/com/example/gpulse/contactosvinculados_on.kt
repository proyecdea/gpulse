package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_contactosvinculados_off.*
import kotlinx.android.synthetic.main.activity_contactosvinculados_off.button13
import kotlinx.android.synthetic.main.activity_contactosvinculados_off.button14
import kotlinx.android.synthetic.main.activity_contactosvinculados_off.button15
import kotlinx.android.synthetic.main.activity_contactosvinculados_off.button16
import kotlinx.android.synthetic.main.activity_contactosvinculados_on.*
import kotlinx.android.synthetic.main.activity_usuariosvinculados_on.*

class contactosvinculados_on : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contactosvinculados_on)

        button13.setOnClickListener{
            val intent: Intent = Intent(this, usuariosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
        button14.setOnClickListener{
            val intent: Intent = Intent(this, mapa::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
        button16.setOnClickListener{
            val intent: Intent = Intent(this, notificaciones::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
        button15.setOnClickListener{
            val intent: Intent = Intent(this, perfil::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
    }
    override fun onBackPressed() {
        val intent: Intent= Intent (Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
