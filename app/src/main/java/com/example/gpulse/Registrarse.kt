package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_ingresar.*
import kotlinx.android.synthetic.main.activity_registrarse.*

class Registrarse : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrarse)

        button6.setOnClickListener {
            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right)
        }
        button4.setOnClickListener {
            val intent: Intent = Intent(this, seleccionar_cuenta::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right, R.anim.slideout_left)
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        val intent: Intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
    }
}
