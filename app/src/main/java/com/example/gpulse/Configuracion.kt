package com.example.gpulse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_configuracion.*
import kotlinx.android.synthetic.main.activity_configuracion.button7
import kotlinx.android.synthetic.main.activity_perfil.*
import kotlinx.android.synthetic.main.activity_perfil.button12
import kotlinx.android.synthetic.main.activity_perfil.button13
import kotlinx.android.synthetic.main.activity_perfil.button14
import kotlinx.android.synthetic.main.activity_perfil.button15
import kotlinx.android.synthetic.main.activity_perfil.button16
import kotlinx.android.synthetic.main.activity_seleccionar_cuenta.*

class Configuracion : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuracion)

        button12.setOnClickListener{
            val intent: Intent = Intent(this, contactosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button13.setOnClickListener{
            val intent: Intent = Intent(this, usuariosvinculados_off::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button14.setOnClickListener{
            val intent: Intent = Intent(this, mapa::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }
        button16.setOnClickListener{
            val intent: Intent = Intent(this, notificaciones::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }

        button15.setOnClickListener{
            val intent: Intent = Intent(this, perfil::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }

        button7.setOnClickListener{
            val intent: Intent = Intent(this, perfil::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
        }

        button17.setOnClickListener{
            val intent: Intent = Intent(this, notificaciones::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }

        button18.setOnClickListener{
            val intent: Intent = Intent(this, perfil::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }

        button19.setOnClickListener{
            val intent: Intent = Intent(this, infoapp::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slidein_right,R.anim.slideout_left)
        }
    }
    override fun onBackPressed() {
        val intent: Intent = Intent(this, perfil::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slidein_left,R.anim.slideout_right)
    }
}
